let cacheFunction = function (cb) {
    let cache = {};
    function cbInvoke() {

        if (cache.hasOwnProperty([[...arguments]])) {
            return cache[[...arguments]]
        }
        else {
            cache[[...arguments]] = cb(...arguments)
            console.log(cache)
        }


        return cb(...arguments)
    }
    return cbInvoke


}

module.exports = cacheFunction
let result = cacheFunction(cb)
result(3)
result(3)
result(4)

